<?php
/**
 * @file
 * This is a template file for a pop-up telling the user the website version.
 *
 * When overriding this template it is important to note that jQuery will use
 * the following class to assign actions to buttons:
 *
 * ignore-button      - closes the popup
 *
 * Variables available:
 * - $message:  Contains the text that will be display whithin the pop-up
 */
?>

<div>
  <div class ="popup-content info">
    <div id="popup-text">
      <?php print $message ?>
    </div>
    <div id="popup-button">
      <div class="text"><?php print t("Ignore"); ?></div><button type="button" class="ignore-button"></button>
    </div>
  </div>
</div>
