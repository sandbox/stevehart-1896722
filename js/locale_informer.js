Drupal.behaviors.locale_informer_popup = function(context) {
  $('body').not('.informer-popup-processed').addClass('informer-popup-processed').each(function() {
    try {
      var enabled = Drupal.settings.locale_informer.popup_enabled;
      if(!enabled) {
        return;
      }
      if (!Drupal.locale_informer.cookiesEnabled()) {
        return;
      }
      var status = Drupal.locale_informer.getCurrentStatus();
      if (status == 0) {
        var next_status = 1;
        $('.ignore-button').click(function(){
          Drupal.locale_informer.changeStatus(next_status);
        });
        Drupal.locale_informer.createPopup(Drupal.settings.locale_informer.popup_html_info);
      } 
      else {
        return;
      }
    }
    catch(e) {
      return;
    }
  });
}

Drupal.locale_informer = {};

Drupal.locale_informer.createPopup = function(html) {
  var position = Drupal.settings.locale_informer.popup_position;
  var popup = $(html)
    .attr({"id": "informer-popup"})
    .hide();
    popup.prependTo(position);
    var height = popup.height();
    popup.show()
      .attr({"class": "informer-popup-top clearfix"})
      .css({"top": -1 * height})
      .animate({top: 0}, 1);
  Drupal.locale_informer.attachEvents();
}

Drupal.locale_informer.attachEvents = function() {
  $('.ignore-button').click(function(){
    Drupal.locale_informer.setStatus(1);
    Drupal.locale_informer.changeStatus(2);
  });
}

Drupal.locale_informer.getCurrentStatus = function() {
  var search = 'cookie-agreed-'+Drupal.settings.locale_informer.popup_language+'=';
  var offset = document.cookie.indexOf(search);
  if (offset < 0) {
    return 0;
  }
  offset += search.length;
  var end = document.cookie.indexOf(';', offset);
  if (end == -1) {
    end = document.cookie.length;
  }
  var value = document.cookie.substring(offset, end);
  return parseInt(value);
}

Drupal.locale_informer.changeStatus = function(value) {
  var status = Drupal.locale_informer.getCurrentStatus();
  if (status == value) return;
      if(status == 0) {
        $("#informer-popup").html(Drupal.settings.locale_informer.popup_html_agreed);
        Drupal.locale_informer.attachEvents();
      }
      if(status == 1) {
        $("#informer-popup").fadeOut(300);
      }
  Drupal.locale_informer.setStatus(value);
}

Drupal.locale_informer.setStatus = function(status) {
  var date = new Date();
  date.setDate(date.getDate() + 100);
  document.cookie = "cookie-agreed-"+Drupal.settings.locale_informer.popup_language + "="+status+";expires=" + date.toUTCString() + ";path=" + Drupal.settings.basePath;
}

Drupal.locale_informer.hasAgreed = function() {
  var status = Drupal.locale_informer.getCurrentStatus();
  if(status == 1) {
    return true;
  }
  return false;
}

Drupal.locale_informer.cookiesEnabled = function() {
  var cookieEnabled = (navigator.cookieEnabled) ? true : false;
    if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
      document.cookie="testcookie";
      cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
   }
   return (cookieEnabled);
}
