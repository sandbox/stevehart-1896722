<?php
/**
 * @file
 * This file provides administration form for the module.
 */

/**
 * Provides form for locale informer popup.
 */
function locale_informer_admin_form($form_state) {

  global $language;
  $ln = $language->language;
  $popup_settings = locale_informer_get_settings();
  $form['locale_informer_' . $ln] = array(
    '#type'  => 'item',
    '#tree'   => TRUE,
  );
  
  $form['locale_informer_' . $ln]['#title'] = t('You are editing settings for the %language language', array('%language' => $language->name));

  $form['locale_informer_' . $ln]['popup_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable popup for this locale'),
    '#default_value' => isset($popup_settings['popup_enabled']) ? $popup_settings['popup_enabled'] : 0,
  );
  
  $form['locale_informer_' . $ln]['popup_text'] = array(
    '#type' => 'textfield',
    '#size' => 80,
    '#title' => t('Popup message'),
    '#default_value' => isset($popup_settings['popup_text']) ? $popup_settings['popup_text'] : '',
    '#required' => TRUE,
    '#description' => t('Inform the user what locale they are viewing'),
  );

  $form['locale_informer_' . $ln]['popup_position'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#title' => t('Popup message position'),
    '#default_value' => isset($popup_settings['popup_position']) ? $popup_settings['popup_position'] : '',
    '#required' => TRUE,
    '#description' => t('Where would you like to see this popup message appear? By default its attached the the body div. To change this please select either a CSS class (.) or and id (#) such as #wrapper or .content'),
  );
  
  $form['locale_informer_' . $ln]['link'] = array(
    '#type' => 'fieldset',
    '#title' => t('Internal link'),
    '#collapsible' => TRUE,
    '#description' => t('If you want to add a link to a page within your website you can here or leave blank if not'),
  );
  
  $form['locale_informer_' . $ln]['link']['popup_link_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($popup_settings['link']['popup_link_title']) ? $popup_settings['link']['popup_link_title'] : '',
    '#size' => 60,
    '#description' => t('Link title'),
  );
  
  $form['locale_informer_' . $ln]['link']['popup_link_des'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination'),
    '#default_value' => isset($popup_settings['link']['popup_link_des']) ? $popup_settings['link']['popup_link_des'] : '',
    '#size' => 60,
    '#maxlength' => 220,
    '#description' => t('Internal page, no "/"'),
  );

  return system_settings_form($form);
}
